import 'dart:ui';
import '../Constants.dart';
import 'package:flutter/material.dart';

enum SIZE { small, medium, big}

class ScreenSize {
  static getSize(context) {
    Size size = MediaQuery.of(context).size;
    print(size.width);
    
    if (size.width > 300 && size.width <= 600) {
    
      return SIZE.small;
    }
    if (size.width > 600) {
    
      return SIZE.medium;
    }

  }

  static setConstants(screensize) {
    if (screensize == SIZE.small) {
      Constants.TINY_FONT = 7;
      Constants.SMALL_FONT = 9;
      Constants.MEDIUM_FONT = 13;
      Constants.LARGE_FONT = 30;
      Constants.HUGE_FONT = 45;
      return;
    }
    if (screensize == SIZE.medium) {
      Constants.TINY_FONT = 10;
      Constants.SMALL_FONT = 18;
      Constants.MEDIUM_FONT = 26;
      Constants.LARGE_FONT = 40;
      Constants.HUGE_FONT = 60;
      return;
    }
  }
}