import 'dart:convert';

import 'package:Fortix_App/RegisterScreen.dart';
import 'package:flutter/services.dart';
import 'package:Fortix_App/components/FilterCard.dart';
import 'package:Fortix_App/components/VisitsCard.dart';
import 'package:Fortix_App/SearchPage.dart';
import 'package:Fortix_App/vo/Visit.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:carousel_slider/carousel_slider.dart';

import 'Constants.dart';
import 'functions/ScreenSize.dart';

class MainScreen extends StatefulWidget {




  static const String route = "/smartcitymain";
  final String id;

  MainScreen(this.id);

  @override
  State<StatefulWidget> createState() {
    return MainScreenState(this.id);
  }
}

class MainScreenState extends State<MainScreen> {

  String id;
  String title = "Home";
  CarouselSlider carouselSlider;
  int _curIndex = 0;
  int _current = 0;
  List imgList = [
    'https://archiv.kompasslev.cz/rabattkompass.at/public/gimg/7/8/3/5/8/3/783583-950-100000.jpg',
    'https://static.vecteezy.com/system/resources/previews/000/568/989/non_2x/vector-abstract-business-commercial-flyer.jpg',
    'https://i.pinimg.com/originals/d6/c7/ff/d6c7ff116cc39e1fd115a216f202c051.png',
    'https://lh3.googleusercontent.com/proxy/veJ2NXfHS_t6V0ATWfdqEVgvIcsSC1FqRGQ1dPCyp2skyO4DfHJCzX9Sdo01cWKl_SBrAQJolbTRLv53AjSMpp1AVHXiW26itLJLKXkX',
    'https://eu003.leafletcdns.com/at/data/27/8563/0_s.jpg?t=1580049755'
  ];

  MainScreenState(String id) {
    this.id = id;
  }

  Color black54 = Color(0x8A000000);
  int amountGrids = 2;
  List<Visit> visits = new List();

  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp
    ]);
  }

  dispose(){
    //Change Orientation after leaving Screen here!
    super.dispose();
  }

  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }
    return result;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Constants.purpleBars,
        title: Row(
          children: <Widget>[
            InkWell(
              child: Icon(Icons.arrow_back),
              onTap: () {
                Navigator.pushReplacementNamed(context, RegisterScreen.route, arguments: this.id);
              },
            ),
            SizedBox(width: 30,),
            Text(title,
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.white
              ),),
          ],
        ),
      ),
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            carouselSlider = CarouselSlider(
              height: 380.0,
              initialPage: 0,
              enlargeCenterPage: true,
              autoPlay: true,
              reverse: false,
              enableInfiniteScroll: true,
              autoPlayInterval: Duration(seconds: 3),
              autoPlayAnimationDuration: Duration(milliseconds: 2500),
              pauseAutoPlayOnTouch: Duration(seconds: 10),
              scrollDirection: Axis.horizontal,
              onPageChanged: (index) {
                setState(() {
                  _current = index;
                });
              },
              items: imgList.map((imgUrl) {
                return Builder(
                  builder: (BuildContext context) {
                    return Container(
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.symmetric(horizontal: 10.0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                      ),
                      child: Image.network(
                        imgUrl,
                        fit: BoxFit.fill,
                      ),
                    );
                  },
                );
              }).toList(),
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: map<Widget>(imgList, (index, url) {
                return Container(
                  width: 10.0,
                  height: 10.0,
                  margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: _current == index ? Colors.blueGrey : Colors.grey,
                  ),
                );
              }),
            ),
          ],
        ),
      ),

      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Home'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.search),
            title: Text('Search'),
          ),
        ],

        unselectedItemColor: Colors.grey,
        selectedItemColor: Colors.white,
        backgroundColor: Constants.purpleBars,

        currentIndex: _curIndex,
        onTap: (index) {
          setState(() {
            _curIndex = index;
            switch (_curIndex) {
              case 0:
                break;
              case 1:
                Navigator.pushReplacementNamed(context, SearchPage.route, arguments: this.id);
                break;
              case 2:
                break;
            }
          });
        },
      ),


    );
  }



}
