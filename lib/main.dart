import 'package:Fortix_App/RegisterScreen.dart';
import 'package:flutter/material.dart';
import 'Router.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
    
  @override
  Widget build(BuildContext context) {
    

    return MaterialApp(
      title: "Fortix_App",
      theme: ThemeData(
        fontFamily: 'Nunito'),
      onGenerateRoute: Router.generateRoute,
      initialRoute: RegisterScreen.route
      
    );
  }
}


