import 'package:Fortix_App/vo/Visit.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:Fortix_App/Constants.dart';

class OfferScreen extends StatefulWidget {
  static const String route = "/locationscreen";
  final Visit visit;

  OfferScreen(this.visit);

  @override
  State<StatefulWidget> createState() {
    return OfferScreenState();
  }
}

class OfferScreenState extends State<OfferScreen> {
  Color black54 = Color(0x8A000000);

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery
        .of(context)
        .size;
    return SafeArea(
        child: Scaffold(
          appBar: AppBar(
            title: Text("Spot Details"),
            elevation: 0,
            backgroundColor: Constants.darkBlue,
          ),
          body: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomLeft,
                  colors: [
                    Constants.azure.withOpacity(0.9),
                    Constants.blue.withOpacity(0.9),
                  ]),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: 20,
                ),
                Text(
                  widget.visit.name,
                  style: TextStyle(
                    fontSize: Constants.LARGE_FONT,
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                  ),
                ),
                Text(
                  widget.visit.description,
                  style: TextStyle(
                    fontSize: Constants.MEDIUM_FONT,
                    fontWeight: FontWeight.w200,
                    color: Colors.white,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(30),
                  child: Text(
                    widget.visit.longDescription,
                    style: TextStyle(
                      fontSize: Constants.SMALL_FONT,
                      fontWeight: FontWeight.w100,
                      color: Colors.white,
                    ),
                  ),
                ),
                ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  child: Image.network(
                    widget.visit.urlToImage,
                    fit: BoxFit.cover,
                    width: size.width * 0.86,
                    height: size.height * 0.2,
                  ),
                ),
                SizedBox(
                  height: size.height * 0.03,
                ),
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Container(
                        child: InkWell(
                          radius: 55,
                          splashColor: Constants.borderGrey,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(
                                Icons.favorite_border,
                                color: Colors.white,
                                size: 50,
                              ),
                              Text(
                                widget.visit.amountAwesome.toString() + " times Awesome",
                                style: TextStyle(
                                    color: Colors.white, fontWeight: FontWeight
                                    .w700),
                              )
                            ],
                          ),
                          onTap: () {
                            print("Awesome!");
                          },
                        ),
                      ),

                      Container(
                          child: InkWell(
                            radius: 55,
                            splashColor: Constants.borderGrey,
                            focusColor: Constants.darkBlue,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                  Icons.favorite_border,
                                  color: Colors.white,
                                  size: 50,
                                ),
                                Text(
                                  widget.visit.amountAwesome.toString() + " times Good",
                                  style: TextStyle(
                                      color: Colors.white, fontWeight: FontWeight
                                      .w700),
                                )
                              ],
                            ),
                            onTap: () {
                              print("Good!");
                            },
                          ),
                      ),


                    ])
              ],
            ),
          ),
        ));
  }
}
