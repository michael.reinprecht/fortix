import 'package:Fortix_App/IDReceiver.dart';
import 'package:Fortix_App/components/LoginButton.dart';
import 'package:Fortix_App/components/RegisterButton.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import "package:Fortix_App/Constants.dart";

import 'MainScreen.dart';

class RegisterScreen extends StatefulWidget {
  static const String route = "/register";

  @override
  State<StatefulWidget> createState() {
    return new RegisterScreenState();
  }
}

class RegisterScreenState extends State<RegisterScreen> {


  final usernameController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    usernameController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  void initState()  {
    super.initState();
     //checkIfAlreadyRegistered();
  }


  /*
  checkIfAlreadyRegistered() async{
     final prefs = await SharedPreferences.getInstance();
    String id = prefs.getString("id");
    if (id!=null){
      print("user id:" + id);
      Navigator.pushNamed(context, SmartCityMain.route, arguments: id);
    } 
  }
   */


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
            begin: Alignment.bottomCenter,
            end: Alignment.topLeft,
            colors: [
              Constants.deepPurple,
              Constants.lightPurple
            ])
          ),
        ),
        Center(
            child: Container(
                padding: EdgeInsets.all(20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Spacer(flex: 6,),
                    Text(
                      "AppName",
                      style: TextStyle(
                        fontSize: 34,
                        color: Colors.white,
                      ),
                    ),
                    Spacer(flex: 2,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Container(
                          height: 80,
                          width: 300,
                          decoration: BoxDecoration(
                            /*
                            border: Border.all(
                              color: Colors.lightBlueAccent,
                            ),
                            borderRadius: BorderRadius.all(new Radius.circular(40)),
                            color: Colors.black12,
                            */
                          ),
                          child: Container(
                            padding: EdgeInsets.fromLTRB(25, 0, 25, 5),
                            child: Center(
                              child: TextField(
                                style: TextStyle(
                                    color: Colors.white
                                ),
                                controller: usernameController,
                                textAlign: TextAlign.left,
                                decoration: InputDecoration(
                                  hintStyle: TextStyle(
                                      color: Colors.white
                                  ),
                                  enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: Colors.white)
                                  ),
                                  border: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.white,
                                    ),
                                  ),
                                  hintText: 'Username',
                                ),
                              ),
                            ),
                          ),


                        ),
                      ],
                    ),
                    Spacer(flex:1),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Container(
                          height: 80,
                          width: 300,
                          decoration: BoxDecoration(
                            /*
                            border: Border.all(
                              color: Colors.lightBlueAccent,
                            ),
                            borderRadius: BorderRadius.all(new Radius.circular(40)),
                            color: Colors.black12,
                             */
                          ),
                          child: Container(
                            padding: EdgeInsets.fromLTRB(25, 0, 25, 8),
                            child: Center(
                              child: TextField(
                                style: TextStyle(
                                  color: Colors.white
                                ),
                                controller: passwordController,
                                textAlign: TextAlign.left,
                                decoration: InputDecoration(
                                    hintStyle: TextStyle(
                                        color: Colors.white
                                    ),
                                    enabledBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(color: Colors.white)
                                    ),
                                    border: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Colors.white,
                                      ),
                                    ),
                                    hintText: 'Password'
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Spacer(flex: 3,),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        LoginButton(
                            usernameController.text, passwordController.text, IDReceiver.route),
                        SizedBox(height: 10,),
                        RegisterButton(
                            usernameController.text, passwordController.text, IDReceiver.route)
                      ],
                    ),
                    Spacer(flex: 4,)
                  ],
                ))),
      ],
    ));
  }
}
