import 'package:Fortix_App/Constants.dart';
import 'package:Fortix_App/RegisterScreen.dart';
import 'package:Fortix_App/MainScreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class IDReceiver extends StatefulWidget {
  static const route = "/idreceiver";
  @override
  State<StatefulWidget> createState() {
    return IDReceiverState();
  }
}

class IDReceiverState extends State<IDReceiver> {

  String text = "Fetching your id...";

  @override
  void initState() {
    super.initState();
    getIdFromServer();
  }

  getIdFromServer() async {
    final prefs = await SharedPreferences.getInstance();
    String id = prefs.getString("id");


    if (id == null) {
      try {
        var response = await http.get(Constants.SERVICE_URL + "getNewId");

        if (response.statusCode <= 200 || response.statusCode < 300) {
          prefs.setString("id", response.body);
          id = response.body;
        } else{
          setState(() {
            this.text = "Service nicht verfügbar! Bitte später nochmal probieren!";
            Navigator.pushNamed(context, RegisterScreen.route);
          });
          return;
        }
      } on Exception catch (e) {
        return;
      }
    }else{
      print("id schon gespeichert");
    }

    Navigator.pushNamed(context, MainScreen.route, arguments: id);
  }

  removeSP() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove("id");
    print("delteed SP");
  }

  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
        body: Center(
            child: GestureDetector(
                onDoubleTap: () {
                  this.getIdFromServer();
                },
                onTap: () {
                  this.removeSP();
                },
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image.asset("assets/wait.gif"),
                    Text(this.text)
                  ],
                ))));
    ;
  }
}
