import 'dart:ui';

import 'package:flutter/material.dart';

class Constants {
  static const String SERVICE_URL = "https://itlabor2.htldornbirn.vol.at/";
  static const String SERVICE_LOCATION = "getLocations";
  static const String SERVICE_OFFER = "offers";

  // Colors
  static const Color black = Colors.black87;
  static const Color black54 = Color(0x8A000000);
  static const Color darkBlueold = Color(0xff030394); // '#030394',
  static const Color lightBlue = Color(0xff1703FF); //'#1703FF',
  static const Color lightGreen = Color(0xff24E394); //'#24E394',
  static const Color strongRed = Color(0xffFF0000); //'#FF0000',
  static const Color softRed = Color(0xffFE384B); //'FE384B',
  static const Color azure = Color(0xff00bfb2); //'#00bfb2',
  static const Color darkBlue = Color(0xff1f363d); //'#1f363d',
  static const Color red = Color(0xffeb5e55); // '#eb5e55',
  static const Color blue = Color(0xff2978a0); //'#2978a0',
  static const Color yellow = Color(0xffedae49); //'#edae49',
  static const Color oldYellow = Color(0xfffed766); //'#fed766',
  static const Color lightGrey = Color(0xfff0f1f2); //'#f0f1f2',
  static const Color borderGrey = Color(0xff86939e); //'#86939e',
  static const Color errorRed = Color(0xffff190c); //'#ff190c',
  static const Color textColor = Color(0xff333); // '#333',
  static const Color blackBars = Color(0xff333333); //
  static const Color deepPurple = Color(0xff380ACF);
  static const Color lightPurple = Color(0xffA056D1);
  static const Color purpleBars = Color(0xff40195A);

  static const int BIG_FONT= 35;

  static double TINY_FONT;
  static double SMALL_FONT;
  static double MEDIUM_FONT;
  static double LARGE_FONT;
  static double HUGE_FONT;

  //VisitsCard
  static const double vc_fontsize_big = 20;
  static const double vc_fontsize_small = 12;
}
