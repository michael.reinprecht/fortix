import 'package:Fortix_App/IDReceiver.dart';
import 'package:Fortix_App/OfferScreen.dart';
import 'package:Fortix_App/RegisterScreen.dart';
import 'package:Fortix_App/MainScreen.dart';
import 'package:Fortix_App/vo/Visit.dart';
import 'package:flutter/cupertino.dart';
import 'package:Fortix_App/SearchPage.dart';
import 'package:flutter/material.dart';

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
     
      case RegisterScreen.route:
        return MaterialPageRoute(builder: (context) => RegisterScreen());
      case SearchPage.route:
        String id = settings.arguments;
        return MaterialPageRoute(builder: (context) => SearchPage(id));
      case IDReceiver.route:
        return MaterialPageRoute(builder: (context) => IDReceiver());
      case OfferScreen.route:
        Visit visit = settings.arguments;
        return MaterialPageRoute(builder: (context) => OfferScreen(visit));
      case MainScreen.route:
        String id = settings.arguments;
        return MaterialPageRoute(builder: (context) => MainScreen(id));
      default:
        return MaterialPageRoute(builder: (context) => RegisterScreen());
    }
  }
}
