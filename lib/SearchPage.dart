import 'dart:convert';

import 'package:Fortix_App/MainScreen.dart';
import 'package:Fortix_App/RegisterScreen.dart';
import 'package:Fortix_App/vo/Offer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'Constants.dart';
import 'components/SearchCard.dart';
import 'functions/ScreenSize.dart';

class SearchPage extends StatefulWidget {
  static const String route = "/cityoffers";
  final String id;
  SearchPage(this.id);



  @override
  State<StatefulWidget> createState() {
    return SearchPageState(this.id);
  }
}

class SearchPageState extends State<SearchPage> {
  Color black54 = Color(0x8A000000);
  int amountGrids = 2;

String id = "";

  final searchController = TextEditingController();

  void dispose() {
    // Clean up the controller when the widget is disposed.
    searchController.dispose();
    super.dispose();
  }


SearchPageState(String id) {
  this.id = id;
}

  @override
  void initState() {
    super.initState();
    /*
    fetchMyOffersFromServer();

     */
  }

  int _curIndex = 1;

  @override
  Widget build(BuildContext context) {
    ScreenSize.setConstants(ScreenSize.getSize(context));
    Size size = MediaQuery.of(context).size;


    Offer offer1 = new Offer();
    offer1.name = "Title1";
    offer1.urlToImage = "https://archiv.kompasslev.cz/rabattkompass.at/public/gimg/7/8/3/5/8/3/783583-950-100000.jpg";
    offer1.startDate = "01.01.2020";
    offer1.endDate = "31.12.2099";
    offer1.title = "Title1";
    offer1.description = "Description of the Product";

    Offer offer2 = new Offer();
    offer2.name = "Title1";
    offer2.urlToImage = "https://static.vecteezy.com/system/resources/previews/000/568/989/non_2x/vector-abstract-business-commercial-flyer.jpg";
    offer2.startDate = "01.01.2020";
    offer2.endDate = "31.12.2099";
    offer2.title = "Title1";
    offer2.description = "Description of the Product";

    Offer offer3 = new Offer();
    offer3.name = "Title1";
    offer3.urlToImage = "https://lh3.googleusercontent.com/proxy/veJ2NXfHS_t6V0ATWfdqEVgvIcsSC1FqRGQ1dPCyp2skyO4DfHJCzX9Sdo01cWKl_SBrAQJolbTRLv53AjSMpp1AVHXiW26itLJLKXkX";
    offer3.startDate = "01.01.2020";
    offer3.endDate = "31.12.2099";
    offer3.title = "Title1";
    offer3.description = "Description of the Product";

    Offer offer4 = new Offer();
    offer4.name = "Title1";
    offer4.urlToImage = "https://eu003.leafletcdns.com/at/data/27/8563/0_s.jpg?t=1580049755";
    offer4.startDate = "01.01.2020";
    offer4.endDate = "31.12.2099";
    offer4.title = "Title1";
    offer4.description = "Description of the Product";

    Offer offer5 = new Offer();
    offer5.name = "Title1";
    offer5.urlToImage = "https://archiv.kompasslev.cz/rabattkompass.at/public/gimg/7/8/3/5/8/3/783583-950-100000.jpg";
    offer5.startDate = "01.01.2020";
    offer5.endDate = "31.12.2099";
    offer5.title = "Title1";
    offer5.description = "Description of the Product";

    List<Widget> OfferList = [
      SearchCard(offer1),
      SearchCard(offer2),
      SearchCard(offer3),
      SearchCard(offer4),
      SearchCard(offer5),
    ];

    return SafeArea(
        child: Scaffold(
          body: Container(
            decoration: BoxDecoration(
              color: Colors.white
            ),

            child: CustomScrollView(
              slivers: <Widget>[
                SliverAppBar(
                  snap: false,
                  flexibleSpace: Container(
                    color: Constants.purpleBars,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.fromLTRB(15, 10, 0, 15),
                            height: size.height*0.15,
                            width: size.width*0.7,
                            child: Container(
                                child: TextField(
                                  style: TextStyle(
                                    fontSize: 18,
                                    color: Colors.white

                                  ),
                                  controller: searchController,
                                  textAlign: TextAlign.left,
                                  decoration: InputDecoration(
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: Colors.white)
                                    ),
                                    focusedBorder: null,
                                    hintStyle: TextStyle(
                                      fontSize: 20,
                                      color: Colors.white
                                    ),
                                      border: UnderlineInputBorder(
                                      ),
                                      hintText: 'Search'
                                  ),
                                ),
                            ),
                          ),
                          SizedBox(width: 15,),
                          Container(
                            padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
                            child: Icon(
                              Icons.search,
                              color: Colors.white,
                              size: 35,
                            ),
                          )

                        ],
                      ),
                  ),
                  expandedHeight: size.height *0.15,
                  pinned: false,
                  backgroundColor: Colors.white,
                  automaticallyImplyLeading: false,
                ),


                SliverList(
                  delegate: SliverChildListDelegate(
                      [Container(
                        height: 35,
                        color: Constants.purpleBars,
                        child: Row(
                          children: <Widget>[
                            Spacer(flex: 1),
                            Icon(
                              Icons.filter_list,
                              size: 25,
                              color: Colors.grey,
                            ),
                            Spacer(flex: 1),
                            Icon(
                              Icons.watch_later,
                              size: 25,
                              color: Colors.grey,
                            ),
                            Spacer(flex: 1),
                            Icon(
                              Icons.room,
                              size: 25,
                              color: Colors.grey,
                            ),
                            Spacer(flex:16),
                          ],
                        ),
                      )]
                  ),
                ),

                SliverGrid(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                  ),
                  delegate: SliverChildListDelegate(
                    OfferList,
                  ),
                ),
              ],
            ),
          ),

          bottomNavigationBar: BottomNavigationBar(
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Icon(Icons.home),
                title: Text('Home'),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.search),
                title: Text('Search'),
              ),
            ],

            unselectedItemColor: Colors.grey,
            selectedItemColor: Colors.white,
            backgroundColor: Constants.purpleBars,

            currentIndex: _curIndex,
            onTap: (index) {
              setState(() {
                _curIndex = index;
                switch (_curIndex) {
                  case 0:
                    Navigator.pushReplacementNamed(context, MainScreen.route, arguments: this.id);
                    break;
                  case 1:
                    break;
                  case 2:
                    break;
                }
              });
            },
          ),
        ));
  }
}
