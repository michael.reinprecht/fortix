class Offer {
  String name;
  String title;
  String description;
  String urlToImage;
  double discount;
  String startDate;
  String endDate;


  Offer(
      {this.name,
        this.title,
        this.description,
        this.urlToImage,
        this.discount,
        this.startDate,
        this.endDate});

  factory Offer.fromJson(Map<String, dynamic> json) {
    Offer o = Offer(
        name: json['name'],
        title: json['title'],
        description: json['description'],
        urlToImage: json['image'],
        discount: json['discount'],
        startDate: json['startDate'],
        endDate: json['endDate']);
    if (o.description == null) {
      o.description = "Description";
    }
    if (o.name == null) {
      o.name = "Location";
    }
    if (o.urlToImage == null) {
      o.urlToImage = " ";
    }
    return o;
  }
}
