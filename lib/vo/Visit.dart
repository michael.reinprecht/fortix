class Visit {
  String name, description, longDescription, urlToImage;
  int amountAwesome, amountGood, count;
  DateTime dateOfVisit;

  Visit({this.name,
      this.description,
      this.longDescription,
      this.urlToImage,
      this.dateOfVisit,
      this.amountGood,
      this.count,
      this.amountAwesome});

  factory Visit.fromJson(Map<String, dynamic> json) {
    Visit v = Visit(
        name: json['name'],
        description: json['shortDescription'],
        urlToImage: json['imageUrl'],
        longDescription: json['description'],
        dateOfVisit: new DateTime(2020, 19, 9),
        amountGood: json['good'],
        count: json['count'],
        amountAwesome: json['awesome']);

    if (v.description == null) {
      v.description = "No description available.";
    }
    if (v.name == null) {
      v.name = "No name available.";
    }
    if (v.urlToImage == null) {
      v.urlToImage = " ";
    }
    return v;
  }
}
