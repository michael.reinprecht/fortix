import 'package:Fortix_App/RegisterScreen.dart';
import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CustomerCard extends StatelessWidget {
  final String id;

  CustomerCard(this.id);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return InkWell(
                onTap: ()async{
                   final prefs = await SharedPreferences.getInstance();
                  prefs.remove("id");
                  Navigator.popAndPushNamed(context, RegisterScreen.route);
                  
                },
                child:Container(
        margin: EdgeInsets.only(left: 25, top: 25, right: 25, bottom: 10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(0)),
              image: DecorationImage(
                fit: BoxFit.fill,
                image: AssetImage("assets/CardLogo.png"),
              ),
            ),
            height: height*0.33,
            width: width*0.83,
            padding: EdgeInsets.all(15),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(width: width*0.29, child: Image.asset("assets/sclogo.png")),
                Container(
                  height: 10,
                ),
                Center(
                    child: Container(
                  padding: EdgeInsets.only(left: 20),
                  child: QrImage(
                    data: this.id,
                    version: QrVersions.auto,
                    size: width*0.29,
                    foregroundColor: Colors.white,
                  ),
                )),
                //Text(this.id)
              ],
            )));
  }
}
