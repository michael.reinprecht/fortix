import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class RegisterButton extends StatefulWidget {
  String username;
  String password;
  String route;
  RegisterButton(this.username, this.password, this.route);

  @override
  State<StatefulWidget> createState() {
    return new RegisterButtonState();
  }
}

class RegisterButtonState extends State<RegisterButton> {
  double elevation = 10;

  static const BG_BASIS_COLOR = Colors.lightBlue;
  static const BG_CLICK_COLOR = Colors.lightGreen;
  Color bgcolor = BG_BASIS_COLOR;

  @override
  void initState() {
    
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTapDown: (d) {
          print(widget.username);
          print(widget.password);
          setState(() {
            this.bgcolor = BG_CLICK_COLOR;
            Navigator.pushReplacementNamed(context, widget.route);
          });
        },
        onTapUp: (q) {
          setState(() {
            this.bgcolor = BG_BASIS_COLOR;
          });
        },
        child: SizedBox(
            height: 75,
            width: 260,
            child: Stack(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.white
                    ),
                    borderRadius: BorderRadius.all(new Radius.circular(10)),
                    color: Colors.transparent,
                  ),
                 child: Center(
                   child: Text(
                     "Register",
                     style: TextStyle(
                       color: Colors.white,
                       fontSize: 18,
                     ),
                   ),
                 ),
                ),

              ],
            )));
  }
}
