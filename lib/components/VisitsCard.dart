import 'package:Fortix_App/OfferScreen.dart';
import 'package:Fortix_App/vo/Visit.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:intl/intl.dart';
import "package:Fortix_App/Constants.dart";

class VisitsCard extends StatelessWidget {
  final Visit visit;
  final bool showDate;

  VisitsCard(this.visit, this.showDate);

  Widget getDateWidget() {
    String value;
    if (showDate) {
      value = new DateFormat("dd-MM-yyyy").format(this.visit.dateOfVisit);
    } else {
      value = "Location";
    }

    return Text(
      value,
      style: TextStyle(
          color: Colors.white,
          fontSize: Constants.SMALL_FONT,
          fontWeight: FontWeight.w100),
    );
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Padding(
      padding: EdgeInsets.all(5),
      child: InkWell(
        onTap: () {
          Navigator.pushNamed(context, OfferScreen.route,
              arguments: this.visit);
        },
        child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(new Radius.circular(10)),
              color: Constants.darkBlue,
            ),
            padding: const EdgeInsets.all(0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.vertical(
                      top: Radius.circular(10), bottom: Radius.circular(0)),
                  child: Image.network(
                    this.visit.urlToImage,
                    height: size.height * 0.1,
                    width: size.width * 0.5,
                    fit: BoxFit.cover,
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(8, 0, 10, 5),
                      child: Container(
                        width: size.width * 0.30,
                        height: size.height * 0.11,
                        alignment: Alignment(-1, 0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            getDateWidget(),
                            Flexible(
                                child: Text(
                              this.visit.name,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: Constants.MEDIUM_FONT,
                                  fontWeight: FontWeight.w800),
                            )),
                            Text(
                              this.visit.description,
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w100,
                                fontSize: Constants.SMALL_FONT,
                              ),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              this.visit.count.toString() + " visits",
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w100,
                                fontSize: Constants.SMALL_FONT,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            )),
      ),
    );
  }
}
