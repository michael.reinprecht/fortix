import 'package:Fortix_App/vo/Offer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import "package:Fortix_App/Constants.dart";

class SearchCard extends StatelessWidget {
  final Offer offer;
  SearchCard(this.offer);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Container(
      margin: EdgeInsets.all(5),
      alignment: AlignmentDirectional.center,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(20)),
        image: DecorationImage(
          fit: BoxFit.cover,
          image: NetworkImage(offer.urlToImage),
        ),
      ),
      child: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[],
          ),
        ],
      ),
    );
  }
}