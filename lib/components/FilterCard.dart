import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../Constants.dart';

class FilterCard extends StatelessWidget {
  final String name;
  FilterCard(this.name);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
        margin: EdgeInsets.only(left:5,right:5),
        child:Container(
            child:Center(child:Text(this.name,style: TextStyle(color: Colors.white /*Color.fromRGBO(217, 95, 0, 1)*/,fontSize: Constants.MEDIUM_FONT,fontWeight: FontWeight.w900),)),
            padding: EdgeInsets.fromLTRB(25,0,25,0),
            margin:EdgeInsets.all(0),
            decoration: BoxDecoration(
              color:Colors.transparent,

              border:Border.all(color: Colors.white),
              borderRadius: new BorderRadius.all(new Radius.circular(size.height*0.045)),
            )));
    /*
    return Container(
        
        margin: EdgeInsets.only(top: 15,left: 5,right:5,bottom: 20),
        padding: EdgeInsets.only(left: 10,right:10),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            border:Border.all(),
            
        ),
        child: Center(
            child: Text(this.name,
                style: TextStyle(color: Colors.grey, fontSize: 15,fontWeight: FontWeight.w200))));
    */
  }
}
