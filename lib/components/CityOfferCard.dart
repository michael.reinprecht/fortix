import 'package:Fortix_App/vo/Offer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import "package:Fortix_App/Constants.dart";

class CityOfferCard extends StatelessWidget {
  final Offer offer;

  CityOfferCard(this.offer);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Container(
      alignment: AlignmentDirectional.center,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(0)),
        image: DecorationImage(
          fit: BoxFit.cover,
          image: NetworkImage(offer.urlToImage),
        ),
      ),
      child: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
            Container(
              width: width,
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        Constants.black,
                        Constants.black
                      ])),
              child: Column(
                children: <Widget>[
                  SizedBox(height: height*0.02,),
                  Center(
                    child: Text(
                      offer.name,
                      style: TextStyle(fontSize: Constants.HUGE_FONT, color: Colors.white),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(height: height*0.04,),
                  Text(
                    offer.title,
                    style: TextStyle(fontSize: Constants.LARGE_FONT, color: Colors.white),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: height*0.03,),
                ],
              ),
            ),
            SizedBox(height: height*0.05,),
            ],
          ),

          Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[

              Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                      colors: [
                        Constants.darkBlue.withOpacity(0.6),
                        Constants.black.withOpacity(1)
                      ])),
                padding: EdgeInsets.fromLTRB(5, 0, 5, 5),
                width: width,
                child: Column(
                  children: <Widget>[
                    SizedBox(height: height*0.01,),
                      Text(
                      "Gültig vom ${offer.startDate} bis zum ${offer.endDate}",
                      style: TextStyle(fontSize: Constants.MEDIUM_FONT, color: Colors.white),
                      textAlign: TextAlign.start,
                      ),
                  ],
                )
              ),
            ],
          ),
        ],
      ),
    );
  }
}